#!/bin/sh

## Define colors
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

## Check if the script is running as root
if [ "$EUID" -ne 0 ]; then
    >&2 echo -e "${RED}Please run pi-setup as root!${NC}"
    exit 2
fi

## Update and upgrade before going ahead
apt -y update && apt -y upgrade

echo -e "${GREEN}Install Javascript development related packages?${NC}"
select yn in "Yes" "No"; do
    case $yn in
        Yes )
	## Add Nodejs repo
	curl -sL https://deb.nodesource.com/setup_12.x | -E bash -

	## Add yarn repo
	curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
	echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
	
	apt -y install git nodejs yarn gcc g++ make build-essential
	git config --global user.email "tpkhera@gmail.com"
	git config --global user.name "Tarun Khera"
	break;;
        No ) break;;
    esac
done

echo -e "${GREEN}Install and set zsh as your default shell?${NC}"
select yn in "Yes" "No"; do
    case $yn in
        Yes )
	apt install -y zsh
	sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
	curl "https://gitlab.com/tpkhera/pi-setup/-/raw/master/aliases.zsh" >> ~/.oh-my-zsh/custom/aliases.zsh
	break;;
        No ) break;;
    esac
done

echo -e "${GREEN}Install Transmission and import config?${NC}"
select yn in "Yes" "No"; do
    case $yn in
        Yes )
	apt install -y transmission
	curl "https://gitlab.com/tpkhera/pi-setup/-/raw/master/transmission-settings.json" -o /etc/transmission-daemon/settings.json
	break;;
        No ) break;;
    esac
done

echo -e "${GREEN}Install Minidlna and import config?${NC}"
select yn in "Yes" "No"; do
    case $yn in
        Yes )
	apt install -y minidlna
	curl "https://gitlab.com/tpkhera/pi-setup/-/raw/master/minidlna.conf" -o /etc/minidlna.conf
	break;;
        No ) break;;
    esac
done

echo "${GREEN} Setup complete!"
